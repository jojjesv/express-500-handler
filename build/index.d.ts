import { Request, Response, NextFunction } from 'express';
declare type CatchHandler = (e: Error, req: Request, res: Response) => void;
/**
 * Registers a catch handler which will be invoked when an endpoint catches an error.
 */
export declare function registerCatchHandler(handler: CatchHandler): () => void;
/**
 * If `endpoint` is a promise, invoke it and catch any errors. Otherwise just invoke it.
 */
export declare function handleCatch(endpoint: any, opts?: {
    sendStatus: boolean;
}): (req: Request, res: Response, next: NextFunction) => Promise<void>;
export {};
