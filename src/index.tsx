import isPromise from 'is-promise';
import { Request, Response, NextFunction } from 'express';

type CatchHandler = (e: Error, req: Request, res: Response) => void;

var catchHandlers = new Set<CatchHandler>()

/**
 * Registers a catch handler which will be invoked when an endpoint catches an error.
 */
export function registerCatchHandler(handler: CatchHandler) {
  catchHandlers.add(handler);

  return () => {
    catchHandlers.delete(handler);
  }
}

function dispatchToCatchHandlers(error: Error, req: Request, res: Response) {
  catchHandlers.forEach((handler) => handler(error, req, res));
}

/**
 * If `endpoint` is a promise, invoke it and catch any errors. Otherwise just invoke it.
 */
export function handleCatch(endpoint: any, opts = {
  sendStatus: false
}) {
  if (typeof endpoint !== "function") {
    throw new Error("[handleCatch] endpoint must be a function")
  }

  return async (req: Request, res: Response, next: NextFunction) => {
    var result = endpoint(req, res, next);

    if (isPromise(result)) {
      try {
        await result
      } catch (e) {
        if (opts.sendStatus) {
          res.sendStatus(500);
        }
        dispatchToCatchHandlers(e, req, res);
      }
    }
  }
}